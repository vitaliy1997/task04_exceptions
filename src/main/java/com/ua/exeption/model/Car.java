package com.ua.exeption.model;

import java.io.IOException;
import java.io.PrintWriter;

public class Car implements AutoCloseable {

    private int wheel;
    private boolean safetyBelt;
    private boolean car;
    private PrintWriter file;
    private static int counterCar = 0;

    public Car(int wheel, boolean safetyBelt, boolean car){
        counterCar++;
        this.wheel=wheel;
        this.safetyBelt=safetyBelt;
        this.car=car;

    }

    public Car (){
        counterCar++;
    }

    public void close() throws IOException , NullPointerException{

        if((wheel!=4) || (safetyBelt!=true) || (car!=true && (counterCar==5))){
            throw new IOException("It is not car stupid ");
        }
        else {
            try {
                System.out.println("We go to travel");
                file.close();
            }
            catch (NullPointerException e){
                System.out.println("Check Null Pointer");
            }
        }
    }
}

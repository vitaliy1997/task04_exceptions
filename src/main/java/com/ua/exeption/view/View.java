package com.ua.exeption.view;

@FunctionalInterface
public interface View {

    public void view();
}

package com.ua.exeption.view;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class AutoClosableException {

    Map<String, String> menu = new LinkedHashMap<>();
    Map<String, View> putMenu = new LinkedHashMap<>();
//    MaineMenu maineMenu = new MaineMenu();
    Scanner scanner = new Scanner(System.in);

    public AutoClosableException() {

        menu.put("1","Auto Closable Exception");
        menu.put("2", "RuntimeException");
        menu.put("3", "Back to MainMenu");
        menu.put("E", "Exit");

        putMenu.put("1", this::pressButton1);
        putMenu.put("2", this::pressButton2);
        putMenu.put("3", this::pressButton3);
    }

    private void pressButton1() {
    }


    private void pressButton2() {
    }

    private void pressButton3() {
//        maineMenu.shows();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                putMenu.get(keyMenu).view();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("E"));
    }

}

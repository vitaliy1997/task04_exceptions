package com.ua.exeption.view;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MaineMenu {
    AutoClosableException ace = new AutoClosableException();
    Exceptions exceptions = new Exceptions();
    Map<String, String> menu = new LinkedHashMap<>();
    Map<String, View> putMenu = new LinkedHashMap<>();
    Scanner scan = new Scanner(System.in);

    public MaineMenu() {

        menu.put("1","Exceptions");
        menu.put("2", "Auto Closable Exceptions");
        menu.put("E", "Exit");

        putMenu.put("1", this::pressButton1);
        putMenu.put("2", this::pressButton2);
    }

    private void pressButton1() {
        exceptions.show();
    }


    private void pressButton2() {
        ace.show();
    }

    //-----------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void shows() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scan.nextLine().toUpperCase();
            try {
                putMenu.get(keyMenu).view();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("E"));
    }


}
package com.ua.exeption.view;

import com.ua.exeption.exceptionFolder.NullPointerException;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Exceptions {

    NullPointerException nullPointerException;
    Map<String, String> menu = new LinkedHashMap<>();
    Map<String, View> putMenu = new LinkedHashMap<>();
    Scanner input = new Scanner(System.in);

    public Exceptions() {

        menu.put("1", "1-Number Format Exception");
        menu.put("2", "2-RuntimeException");
        menu.put("E", "Exit");

        putMenu.put("1", this::pressButton1);
        putMenu.put("2", this::pressButton2);
    }

    private void pressButton1() {
        new NullPointerException();
        nullPointerException.doSomething(0);
    }


    private void pressButton2() {
    }

    //----------------------------------------------------
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                putMenu.get(keyMenu).view();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("E"));
    }

}

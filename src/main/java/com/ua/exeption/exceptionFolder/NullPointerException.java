package com.ua.exeption.exceptionFolder;

public class NullPointerException {

   public int doSomething(int n) {

        try {
            return 100 / n;

        } catch (ArithmeticException e) {

            System.out.println("Division by zero");
            return 0;
        }
    }

}

package com.ua.exeption.exceptionFolder;

public class RuntimeException extends java.lang.RuntimeException {

    public RuntimeException (String message){
        super(message);
    }
}
